# U-Boot: Pinetab2 (Mainline uboot) based on Kwiboo's work.
# Maintainer: Furkan Kardame <f.kardame@manjaro.org>

pkgname=uboot-pinetab2
pkgver=2023.07_rc4
pkgrel=2
pkgdesc="U-Boot for PineTab2"
arch=('aarch64')
url='http://www.denx.de/wiki/U-Boot/WebHome'
license=('GPL')
makedepends=('dtc' 'bc' 'git' 'python' 'python-pyelftools' 'python-setuptools' 'swig')
provides=('uboot')
conflicts=('uboot')
install=${pkgname}.install
_commit_uboot="2e0e11b8e65c48a43270f4ec4a88b74c8a83e269"
_commit_rkbin="d6ccfe401ca84a98ca3b85c12b9554a1a43a166c"
_srcname_uboot="u-boot-rockchip-$_commit_uboot"
_srcname_rkbin="rkbin-$_commit_rkbin"
source=("https://github.com/Kwiboo/u-boot-rockchip/archive/${_commit_uboot}.tar.gz"
	"https://github.com/rockchip-linux/rkbin/archive/${_commit_rkbin}.tar.gz"
	"config"
        )
sha256sums=('c7eb14a3b3f07849b6848012280dc8ea3df59f64200c38668a14c9972c96eec4'
            '75c23b19a91bd5589cde6abdde6ae20263b05ec5558f844c6ba82a0719cc39e9'
            '1db2e3281f19cf7a9b3075c6437cd646eedde9d30c9556e4131ed9e15beede10')

prepare() {
  cd "${srcdir}/${_srcname_uboot}"
  cp ${srcdir}/config .config
}

build() {
  # Avoid build warnings by editing a .config option in place instead of
  # appending an option to .config, if an option is already present
  update_config() {
    if ! grep -q "^$1=$2$" .config; then
      if grep -q "^# $1 is not set$" .config; then
        sed -i -e "s/^# $1 is not set$/$1=$2/g" .config
      elif grep -q "^$1=" .config; then
        sed -i -e "s/^$1=.*/$1=$2/g" .config
      else
        echo "$1=$2" >> .config
      fi
    fi
  }

  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS
  

  echo -e "\nBuilding U-Boot for Pine64 PineTab 2...\n"
  cd "${srcdir}/${_srcname_uboot}"

  update_config 'CONFIG_IDENT_STRING' '" Manjaro Linux ARM"'
 
  make EXTRAVERSION=-${pkgrel} \
    BL31=../$_srcname_rkbin/bin/rk35/rk3568_bl31_v1.42.elf \
    ROCKCHIP_TPL=../$_srcname_rkbin/bin/rk35/rk3566_ddr_1056MHz_v1.16.bin
}

package() {
  cd "${srcdir}/${_srcname_uboot}"
  mkdir -p "${pkgdir}/boot/extlinux"

  install -D -m 0644 idbloader.img u-boot.itb -t "${pkgdir}/boot"
}
